let collection = [];

function print() {
  return collection;
}

function enqueue(element) {
  collection[collection.length] = element;
  return collection;
}

function dequeue() {
  let newCollection = [];
  for (let i = 1; collection.length > i; i++) {
    newCollection[newCollection.length] = collection[i];
  }
  collection = newCollection;

  return collection;
}

function front() {
  if (isEmpty()) {
    return undefined;
  }
  return collection[0];
}

function size() {
  let count = 0;
  for (let i = 0; i < collection.length; i++) {
    count++;
  }
  return count;
}

function isEmpty() {
  return size() === 0;
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
